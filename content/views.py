from django.shortcuts import render
from django.http.response import HttpResponse, Http404
from django.template.loader import get_template
from django.template import Context
from menu.models import Menu
from sub_menu.models import Sub_Menu
from django.shortcuts import render_to_response, redirect
import re
#import xmltodict as xmltodict
from category_menu.models import Category_Menu
from content.models import Content, Comments, Category, language
from geolocation.models import Geolocation, Views
from django.core.exceptions import ObjectDoesNotExist
from django.core.context_processors import csrf
from django.core.paginator import Paginator
from django.contrib import auth
from django.contrib.auth import logout
from datetime import datetime, time, date, timedelta
import json
from django.http import StreamingHttpResponse
from django.contrib.auth import authenticate
from django.db.models import Q
import requests
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.utils import translation
from django.template import *
from django.contrib.gis.geoip import GeoIP
import xmltodict as xmltodict
from metar import Metar
from urllib.request import urlopen
from django.core.mail import send_mail, BadHeaderError

""" *****---- get last 3 month ----*****"""
last_three_month = date.today() - timedelta(days=90)

lang_dic = {"en": 2, "ru": 1}

def auth_required(function):
    def check_auth(request):
        if not request.user.is_authenticated():
            return HttpResponseRedirect('/error/')
        return function(request)
    return check_auth


def check_lang(request):
    lang = getLangSession(request)
    URL = '/ru/'
    if lang == '2':
        URL = '/en/'
    if lang == '1':
        URL = '/ru/'
    return redirect(URL)


def get_news(request, lgn, error=''):
    #lang = getLangSession(request)
    lang = lang_dic[lgn]
    request.session['0'] = lang
    args = {}
    photo = []
    args.update(csrf(request))
    args['error_login'] = error
    args['LANG'] = lgn
    args.update(getMenu_and_Lang(request, lgn))
    args['news'] = Content.objects.filter(created__gt=last_three_month, cat_id=3, language=lang, state=1).order_by('-created')
    contents = Content.objects.filter(cat_id=3, language=lang, state=1).order_by('-id')
    for content in contents:
        # ----- get img link from object ---#
            pat = re.compile(r'<img [^>]*src="([^"]+)')
            photo.append({'id': content.id, 'images': pat.findall(content.full_text)})
        # -----------------------------------#
    args['photos'] = photo
    args['username'] = auth.get_user(request).username
    """ *****---- meteo and exchange info ----*****"""
    args['exchange'] = getExchangeInfo(request)
    args['meteo'] = getMeteoInfo(request);
    """" *****---- geo location info ----***** """
    args.update(getVisitorInfo(request))
    """" *****---- end geo location info ----***** """
    return render_to_response('home.html', args, context_instance=RequestContext(request))

# @auth_required  имеет доступ только авторизованные пользователи
def about_us(request, lgn):
    #lang = getLangSession(request)
    lang = lang_dic[lgn]
    args = {}
    args.update(csrf(request))
    args.update(getMenu_and_Lang(request, lgn))
    args['LANG'] = lgn
    args['about'] = Content.objects.filter(cat_id=4, language=lang, state=1).order_by('-id')
    args['username'] = auth.get_user(request).username
    """ *****---- meteo and exchange info ----*****"""
    args['exchange'] = getExchangeInfo(request)
    args['meteo'] = getMeteoInfo(request);
    """" *****---- geo location info ----***** """
    args.update(getVisitorInfo(request))
    """" *****---- end geo location info ----***** """
    return render_to_response('about_us.html', args, context_instance=RequestContext(request))


def news(request, lgn, content_id):
    #lang = getLangSession(request)
    lang = lang_dic[lgn]  
    args = {}
    args.update(csrf(request))
    args.update(getMenu_and_Lang(request, lgn))
    args['LANG'] = lgn
    args['news_menu'] = Content.objects.filter(~Q(id=content_id), created__gt=last_three_month, cat_id=3, language=lang, state=1).order_by('-created')
    args['news'] = Content.objects.filter(id=content_id, language=lang, state=1)
    if len(args['news']) < 1:
        return redirect("/")
    args['username'] = auth.get_user(request).username
    """ *****---- meteo and exchange info ----*****"""
    args['exchange'] = getExchangeInfo(request)
    args['meteo'] = getMeteoInfo(request);
    """" *****---- geo location info ----***** """
    args.update(getVisitorInfo(request))
    """" *****---- end geo location info ----***** """
    # ----- get img link from object ---#
    imgs = Content.objects.filter(id=content_id, language=lang, state=1)
    for img in imgs:
        pat = re.compile(r'<img [^>]*src="([^"]+)')
        args['photos'] = pat.findall(img.full_text)
    # -----------------------------------#
    return render_to_response('news.html', args, context_instance=RequestContext(request))


def news_archive(request, lgn, page_number=1, year="0", month="0"):
    #lang = getLangSession(request)
    month_dic = {"01": "January", "02": "February", "03": "March", "04": "April", "05": "May", "06": "June",
                 "07": "July", "08": "August", "09": "September", "10": "October", "11": "November", "12": "December"}
    lang = lang_dic[lgn]    
    args = {}
    args.update(csrf(request))
    args.update(getMenu_and_Lang(request, lgn))
    filter_month = Content.objects.filter(cat_id=3, language=lang, state=1).order_by('-created')
    if year == "0" and month == "0":
        all_news = Content.objects.filter(cat_id=3, language=lang, state=1).order_by('-created')
    else:
        all_news = Content.objects.filter(cat_id=3, language=lang, state=1, created__year=year, created__month=month).order_by('-created')
        args['year'] = year
        args['month'] = month
    if len(all_news) < 1:
        all_news = Content.objects.filter(cat_id=3, language=lang, state=1).order_by('-created')
    current_page = Paginator(all_news, 3)
    if int(page_number) > current_page.num_pages:
        page_number = current_page.num_pages
    args['filter_month'] = filter_month
    args['month_dic'] = month_dic
    args['all_news'] = all_news
    args['LANG'] = lgn
    args['current_page'] = int(page_number)
    args['from_page'] = int(int(page_number) - 1)
    args['to_page'] = int(int(page_number) + 2)
    args['news_archive'] = current_page.page(page_number)
    """ *****---- meteo and exchange info ----*****"""
    args['exchange'] = getExchangeInfo(request)
    args['meteo'] = getMeteoInfo(request);
    """" *****---- geo location info ----***** """
    args.update(getVisitorInfo(request))
    """" *****---- end geo location info ----***** """
    # ----- get img link from object ---#
    imgs = Content.objects.filter(cat_id=3, language=lang, state=1).order_by('id')
    for img in imgs:
        pat = re.compile(r'<img [^>]*src="([^"]+)')
        args['photos'] = pat.findall(img.full_text)
        # -----------------------------------#
    return render_to_response('news_archive.html', args, context_instance=RequestContext(request))


def service(request, lgn):
    #lang = getLangSession(request)
    lang = lang_dic[lgn]
    args = {}
    args.update(csrf(request))
    args.update(getMenu_and_Lang(request, lgn))
    args['LANG'] = lgn
    args['services'] = Content.objects.filter(cat_id=6, language=lang, state=1).order_by('-id')
    args['username'] = auth.get_user(request).username
    """ *****---- meteo and exchange info ----*****"""
    args['exchange'] = getExchangeInfo(request)
    args['meteo'] = getMeteoInfo(request);
    """" *****---- geo location info ----***** """
    args.update(getVisitorInfo(request))
    """" *****---- end geo location info ----***** """
    return render_to_response('services.html', args, context_instance=RequestContext(request))


def portfolio(request, lgn):
    #lang = getLangSession(request)
    lang = lang_dic[lgn]
    args = {}
    args.update(csrf(request))
    args.update(getMenu_and_Lang(request, lgn))
    args['LANG'] = lgn
    args['porfolios'] = Content.objects.filter(cat_id=7, language=lang, state=1).order_by('-id')
    args['username'] = auth.get_user(request).username
    """ *****---- meteo and exchange info ----*****"""
    args['exchange'] = getExchangeInfo(request)
    args['meteo'] = getMeteoInfo(request);
    """" *****---- geo location info ----***** """
    args.update(getVisitorInfo(request))
    """" *****---- end geo location info ----***** """
    return render_to_response('portfolio.html', args, context_instance=RequestContext(request))


def lecca(request, lgn, content_id):
    lang = lang_dic[lgn]
    args = {}
    args.update(csrf(request))
    args.update(getMenu_and_Lang(request, lgn))
    args['LANG'] = lgn
    args['Lecca'] = Content.objects.filter(id=content_id, state=1)
    args['username'] = auth.get_user(request).username
    """ *****---- meteo and exchange info ----*****"""
    args['exchange'] = getExchangeInfo(request)
    args['meteo'] = getMeteoInfo(request);
    """" *****---- geo location info ----***** """
    args.update(getVisitorInfo(request))
    """" *****---- end geo location info ----***** """
    return render_to_response('lecca.html', args, context_instance=RequestContext(request))


def contact(request, lgn):
    lang = lang_dic[lgn]
    args = {}
    args.update(csrf(request))
    args.update(getMenu_and_Lang(request, lgn))
    args['LANG'] = lgn
    args['username'] = auth.get_user(request).username
    """ *****---- meteo and exchange info ----*****"""
    args['exchange'] = getExchangeInfo(request)
    args['meteo'] = getMeteoInfo(request);
    """" *****---- geo location info ----***** """
    args.update(getVisitorInfo(request))
    """" *****---- end geo location info ----***** """
    return render_to_response('contact.html', args, context_instance=RequestContext(request))


def details(request, lgn, content_id, menu_id=0):
    #lang = getLangSession(request)
    lang = lang_dic[lgn]
    args = {}
    args.update(csrf(request))
    args.update(getMenu_and_Lang(request, lgn))
    args['LANG'] = lgn
    args['submenus'] = Sub_Menu.objects.filter(cat_id=menu_id)
    args['details'] = Content.objects.filter(id=content_id, language=lang, state=1)
    if len(args['details']) < 1:
        return redirect("/")
    args['request'] = request
    args['username'] = auth.get_user(request).username
    """ *****---- meteo and exchange info ----*****"""
    args['exchange'] = getExchangeInfo(request)
    args['meteo'] = getMeteoInfo(request);
    """" *****---- geo location info ----***** """
    args.update(getVisitorInfo(request))
    """" *****---- end geo location info ----***** """
    return render_to_response('details.html', args, context_instance=RequestContext(request))


def lscm_registration(request, lgn, content_id, menu_id=0):
    #lang = getLangSession(request)
    lang = lang_dic[lgn]
    args = {}
    args.update(csrf(request))
    args.update(getMenu_and_Lang(request, lgn))
    args['LANG'] = lgn
    args['submenus'] = Sub_Menu.objects.filter(cat_id=menu_id)
    args['details'] = Content.objects.filter(id=content_id, language=lang, state=1)
    args['request'] = request
    args['username'] = auth.get_user(request).username
    """ *****---- meteo and exchange info ----*****"""
    args['exchange'] = getExchangeInfo(request)
    args['meteo'] = getMeteoInfo(request);
    """" *****---- geo location info ----***** """
    args.update(getVisitorInfo(request))
    """" *****---- end geo location info ----***** """
    return render_to_response('lscm_registration.html', args, context_instance=RequestContext(request))


def userauthenticate(request):
    username = request.POST['username']
    password = request.POST['password']
    user = auth.authenticate(username=username, password=password)
    if user is not None:
        # Правильный пароль и пользователь "активен"
        auth.login(request, user)
        # Перенаправление на "правильную" страницу
        return HttpResponseRedirect('/')
    else:
        # Отображение страницы с ошибкой
        return HttpResponseRedirect('/error/')


def user_logout(request):
    auth.logout(request)
    request.session['0'] = 0
    # Redirect to a success page.
    return HttpResponseRedirect('/')


def usersignin(request):
    username = request.POST['username_signin']
    password = request.POST['password_signin']
    email = request.POST['email_signin']
    user = User.objects.create_user(username, email, password)
    user.is_staff = True
    user.save()
    user = auth.authenticate(username=username, password=password)
    if user is not None:
        # Правильный пароль и пользователь "активен"
        auth.login(request, user)
        # Перенаправление на "правильную" страницу
    return HttpResponseRedirect('/')


def getlang(request, lang):
    request.session['0'] = lang
    # back_url = request.META['HTTP_REFERER'] #Перенаправляет на текущую страницу
    back_url = '/'
    return redirect(back_url)


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return "80.247.32.206"


def get_location(request):
    g = GeoIP()
    #location = g.country('reserve.kg')
    ip = get_client_ip(request)
    location = g.country(ip)
    return location

def save_locations(ip_address, country_name, country_code):
    SaveVisit = Views.objects.filter(ip_address=ip_address, view_date=date.today()).count()
    if SaveVisit == 0:
        SaveVisit = Geolocation(ip_address=ip_address, country_name=country_name, country_code=country_code, visit_date=date.today())
        SaveVisit.save()
    num_result = Views.objects.filter(ip_address=ip_address, view_date=date.today()).count()
    if num_result > 0:
        saveViews = Views.objects.get(ip_address=ip_address, view_date=date.today())
        saveViews.views_count += 1
        saveViews.save()
    else:
        saveViews = Views(ip_address=ip_address, views_count=1, view_date=date.today())
        saveViews.save()

def getVisits():
    args = {}
    all_location = Geolocation.objects.raw(
        'SELECT id, ip_address, country_name, country_code, visit_date FROM visits '
        'NATURAL JOIN (SELECT country_code, MAX(id) AS id FROM visits GROUP BY country_code) p ORDER BY id DESC LIMIT 5')
    args['all_visitors_location'] = all_location
    args['visitors_count'] = Geolocation.objects.all().count()
    args['visitors_today'] = Geolocation.objects.filter(visit_date=date.today()).count()
    return args


def getViews(param):
    from django.db.models import Sum
    if param == 1:
        views = Views.objects.aggregate(Sum('views_count'))
    elif param == 2:
        views = Views.objects.filter(view_date=date.today).aggregate(Sum('views_count'))
    return views


def getVisitorInfo(request):
    args = {}
    location = get_location(request)
    ip_address = get_client_ip(request)
    country_name = location['country_name']
    country_code = location['country_code']
    save_locations(ip_address, country_name, country_code)
    args['Views'] = getViews(1)
    args['ViewsToday'] = getViews(2)
    visitors_args = getVisits()
    args['all_visitors_location'] = visitors_args['all_visitors_location']
    args['visitors_count'] = visitors_args['visitors_count']
    args['visitors_today'] = visitors_args['visitors_today']
    return args


def getMenu_and_Lang(request, lgn):
    #lang = getLangSession(request)
    lang = lang_dic[lgn]
    args = {}
    args.update(csrf(request))
    args['LANG'] = lgn
    args['catmenus'] = Category_Menu.objects.filter(language=lang)
    args['menus'] = Menu.objects.all()
    args['languages'] = language.objects.filter(~Q(id=lang))
    args['langs'] = language.objects.filter(id=lang)
    return args


def getLangSession(request):
    if '0' not in request.session or request.session['0'] == 0:
        request.session['0'] = 2
    return request.session['0']

# Exchange Rate Info
def getExchangeInfo(request):
    cur = {}
    try:
        r = requests.get('http://www.nbkr.kg/XML/daily.xml')
        data = xmltodict.parse(r.text)
        if 'CurrencyRates' in data:

            cur.update(csrf(request))
            cur['date'] = data['CurrencyRates']['@Date']
            if data['CurrencyRates']['Currency'][0]['@ISOCode'] == 'USD':
                cur['usd'] = data['CurrencyRates']['Currency'][0]['Value']
            if data['CurrencyRates']['Currency'][1]['@ISOCode'] == 'EUR':
                cur['euro'] = data['CurrencyRates']['Currency'][1]['Value']
            if data['CurrencyRates']['Currency'][2]['@ISOCode'] == 'KZT':
                cur['kzt'] = data['CurrencyRates']['Currency'][2]['Value']
            if data['CurrencyRates']['Currency'][3]['@ISOCode'] == 'RUB':
                cur['rub'] = data['CurrencyRates']['Currency'][3]['Value']
    except:
        cur['date'] = date.today
        cur['usd'] = 0.0
        cur['euro'] = 0.0
        cur['kzt'] = 0.0
        cur['rub'] = 0.0
    return cur;

# Meteo Data
def getMeteoInfo(request):

    cur = {}
    cur.update(csrf(request))
    # Get Today Temperature Info
    URL = 'http://tgftp.nws.noaa.gov/data/observations/metar/stations/UCFM.TXT'
    f = urlopen(URL)
    d = f.read()
    metar_text = str(d[16:-1].strip(), 'utf-8')
    m = Metar.Metar(metar_text)
    cur['temperature'] = str(m.temp)
    # Get 5 Days Weather Info
    response = requests.get("http://worldweather.wmo.int/en/json/210_en.xml")
    data = response.json()
    today = data['city']['forecast']['forecastDay']
    cur['citiname'] = data['city']['cityName']
    for i in range(0, len(today)):
            data['city']['forecast']['forecastDay'][i]['forecastDate'] = datetime.strptime(data['city']['forecast']['forecastDay'][i]['forecastDate'], "%Y-%m-%d")
    cur['weather'] = data
    return cur

# Sending Message To Office
def send_email(request):
    subject = request.POST.get('subject', '')
    message = request.POST.get('message', '')
    from_email = request.POST.get('from_email', '')

    if subject and message and from_email:
        try:
            send_mail(subject, message, from_email, ['office@glotechsol.com'])
        except BadHeaderError:
            return HttpResponse('Invalid header found.')
        return HttpResponse('Make sure all fields are entered and valid.')
    else:
        # In reality we'd use a form class
        # to get proper validation errors.
        return HttpResponse('Make sure all fields are entered and valid.')


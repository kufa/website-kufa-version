<?php
require_once(__DIR__ . '/Metar.php');

class Meteo {
	
	public function getMeteoInfo() {
		$get_json = file_get_contents("http://worldweather.wmo.int/en/json/210_en.xml");
		$meteo_obj = json_decode($get_json);		
		return $meteo_obj;
	}
	
	public function getExchangeInfo() {
		$pars = new SimpleXMLElement('http://www.nbkr.kg/XML/daily.xml', null, TRUE);
		return $pars;
	}
	
	
	public function CurrentTemperature() {
		$obj = file_get_contents("http://tgftp.nws.noaa.gov/data/observations/metar/stations/UCFM.TXT");
		$string = trim(substr($obj, 16, strlen($obj)));
		$metar = new \metar_taf\Metar($string);
		return $metar;
	}
	
}

?>